==========================================================
== If you running Sngine v 2.4 you can upgrade to 2.4.1 ==
==========================================================

**Backup your database and your website files just in case something wrong happened**

1- Shutdown your system from the admin panel
2- Upload all files located in the "Update_2.4.1" folder to your live server and replace all files/folders
3- Empty both folders "content/themes/default/templates_compiled" and "content/themes/default/cache"
4- Remove any browser cache and cookies
5- Go to your new admin panel to turn your system back (http://yourdomain.com/path_to_script/admincp) -not "/admin" changed to "/admincp-"

And That's it, Have Fun :)


==================================
== Common Problems After Update ==
==================================

In case you have problem in Translation?
- Copy "gettext.inc" file from "Update 2.4 to 2.4.1" folder to 'includes/libs/gettext' folder
