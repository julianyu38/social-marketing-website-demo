<?php
/**
 * Sngine updater
 * 
 * @package Sngine
 * @author Zamblek
 */

// set system version
define('SYS_VER', '2.5');


// set absolut & base path
define('ABSPATH',dirname(__FILE__).'/');
define('BASEPATH',dirname($_SERVER['PHP_SELF']));


// check the config file
if(!file_exists(ABSPATH.'includes/config.php')) {
    /* the config file doesn't exist -> start the installer */
    header('Location: ./install');
}


// get system configurations
require_once(ABSPATH.'includes/config.php');


// enviroment settings
if(DEBUGGING) {
    ini_set("display_errors", true);
    error_reporting(E_ALL ^ E_NOTICE);
} else {
    ini_set("display_errors", false);
    error_reporting(0);
}


// get functions
require_once(ABSPATH.'includes/functions.php');


// connect to the database
$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$db->set_charset('utf8');
if(mysqli_connect_error()) {
    _error(DB_ERROR);
}


// install
if(isset($_POST['submit'])) {

    // [0] check valid purchase code
    /* get licence key */
    try {
        $licence_key = get_licence_key($_POST['purchase_code']);
        if(is_empty($_POST['purchase_code']) || $licence_key === false) {
            _error("Error", "Please enter a valid purchase code");
        }
        /* update session hash for AJAX CSRF security */
        $session_hash = $licence_key;
    } catch (Exception $e) {
        _error("Error", $e->getMessage());
    }
    
    
    // [2] update the sngine tables
    $structure = "

CREATE TABLE `emojis` (
  `emoji_id` int(10) unsigned NOT NULL auto_increment,
  `pattern` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  PRIMARY KEY (`emoji_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

INSERT INTO `emojis` (`emoji_id`, `pattern`, `class`) VALUES
(1, ':)', 'smile'),
(2, '(&lt;', 'joy'),
(3, ':D', 'smiley'),
(4, ':(', 'worried'),
(5, ':relaxed:', 'relaxed'),
(6, ':P', 'stuck-out-tongue'),
(7, ':O', 'open-mouth'),
(8, ':/', 'confused'),
(9, ';)', 'wink'),
(10, ';(', 'sob'),
(11, 'B|', 'sunglasses'),
(12, ':disappointed:', 'disappointed'),
(13, ':yum:', 'yum'),
(14, '^_^', 'grin'),
(15, ':no_mouth:', 'no-mouth'),
(16, '*_*', 'heart-eyes'),
(17, '*)', 'kissing-heart'),
(18, 'O:)', 'innocent'),
(19, ':angry:', 'angry'),
(20, ':rage:', 'rage'),
(21, ':smirk:', 'smirk'),
(22, ':flushed:', 'flushed'),
(23, ':satisfied:', 'satisfied'),
(24, ':relieved:', 'relieved'),
(25, ':sleeping:', 'sleeping'),
(26, ':stuck_out_tongue:', 'stuck-out-tongue'),
(27, ':stuck_out_tongue_closed_eyes:', 'stuck-out-tongue-closed-eyes'),
(28, ':frowning:', 'frowning'),
(29, ':anguished:', 'anguished'),
(30, ':open_mouth:', 'open-mouth'),
(31, ':grimacing:', 'grimacing'),
(32, ':hushed:', 'hushed'),
(33, ':expressionless:', 'expressionless'),
(34, ':unamused:', 'unamused'),
(35, ':sweat_smile:', 'sweat-smile'),
(36, ':sweat:', 'sweat'),
(37, ':confounded:', 'confounded'),
(38, ':weary:', 'weary'),
(39, ':pensive:', 'pensive'),
(40, ':fearful:', 'fearful'),
(41, ':cold_sweat:', 'cold-sweat'),
(42, ':persevere:', 'persevere'),
(43, ':cry:', 'cry'),
(44, ':astonished:', 'astonished'),
(45, ':scream:', 'scream'),
(46, ':mask:', 'mask'),
(47, ':tired_face:', 'tired-face'),
(48, ':triumph:', 'triumph'),
(49, ':dizzy_face:', 'dizzy-face'),
(50, ':imp:', 'imp'),
(51, ':smiling_imp:', 'smiling-imp'),
(52, ':neutral_face:', 'neutral-face'),
(53, ':alien:', 'alien'),
(54, ':yellow_heart:', 'yellow-heart'),
(55, ':blue_heart:', 'blue-heart'),
(56, ':blue_heart:', 'blue-heart'),
(57, ':heart:', 'heart'),
(58, ':green_heart:', 'green-heart'),
(59, ':broken_heart:', 'broken-heart'),
(60, ':heartbeat:', 'heartbeat'),
(61, ':heartpulse:', 'heartpulse'),
(62, ':two_hearts:', 'two-hearts'),
(63, ':revolving_hearts:', 'revolving-hearts'),
(64, ':cupid:', 'cupid'),
(65, ':sparkling_heart:', 'sparkling-heart'),
(66, ':sparkles:', 'sparkles'),
(67, ':star:', 'star'),
(68, ':star2:', 'star2'),
(69, ':dizzy:', 'dizzy'),
(70, ':boom:', 'boom'),
(71, ':exclamation:', 'exclamation'),
(72, ':anger:', 'anger'),
(73, ':question:', 'question'),
(74, ':grey_exclamation:', 'grey-exclamation'),
(75, ':grey_question:', 'grey-question'),
(76, ':zzz:', 'zzz'),
(77, ':dash:', 'dash'),
(78, ':sweat_drops:', 'sweat-drops'),
(79, ':notes:', 'notes'),
(80, ':musical_note:', 'musical-note'),
(81, ':fire:', 'fire'),
(82, ':poop:', 'poop'),
(83, ':thumbsup:', 'thumbsup'),
(84, ':thumbsdown:', 'thumbsdown'),
(85, ':ok_hand:', 'ok-hand'),
(86, ':punch:', 'punch'),
(87, ':fist:', 'fist'),
(88, ':v:', 'v'),
(89, ':wave:', 'wave'),
(90, ':hand:', 'hand'),
(91, ':raised_hand:', 'raised-hand'),
(92, ':open_hands:', 'open-hands'),
(93, ':point_up:', 'point-up'),
(94, ':point_down:', 'point-down'),
(95, ':point_left:', 'point-left'),
(96, ':point_right:', 'point-right'),
(97, ':raised_hands:', 'raised-hands'),
(98, ':pray:', 'pray'),
(99, ':clap:', 'clap'),
(100, ':muscle:', 'muscle'),
(101, ':runner:', 'runner'),
(102, ':couple:', 'couple'),
(103, ':family:', 'family'),
(104, ':two_men_holding_hands:', 'two-men-holding-hands'),
(105, ':two_women_holding_hands:', 'two-women-holding-hands'),
(106, ':dancer:', 'dancer'),
(107, ':dancers:', 'dancers'),
(108, ':ok_woman:', 'ok-woman'),
(109, ':no_good:', 'no-good'),
(110, ':information_desk_person:', 'information-desk-person'),
(111, ':bride_with_veil:', 'bride-with-veil'),
(112, ':couplekiss:', 'couplekiss'),
(113, ':couple_with_heart:', 'couple-with-heart'),
(114, ':nail_care:', 'nail-care'),
(115, ':boy:', 'boy'),
(116, ':girl:', 'girl'),
(117, ':woman:', 'woman'),
(118, ':man:', 'man'),
(119, ':baby:', 'baby'),
(120, ':older_woman:', 'older-woman'),
(121, ':older_man:', 'older-man'),
(122, ':cop:', 'cop'),
(123, ':angel:', 'angel'),
(124, ':princess:', 'princess'),
(125, ':smiley_cat:', 'smiley-cat'),
(126, ':smile_cat:', 'smile-cat'),
(127, ':heart_eyes_cat:', 'heart-eyes-cat'),
(128, ':kissing_cat:', 'kissing-cat'),
(129, ':smirk_cat:', 'smirk-cat'),
(130, ':scream_cat:', 'scream-cat'),
(131, ':crying_cat_face:', 'crying-cat-face'),
(132, ':joy_cat:', 'joy-cat'),
(133, ':pouting_cat:', 'pouting-cat'),
(134, ':japanese_ogre:', 'japanese-ogre'),
(135, ':see_no_evil:', 'see-no-evil'),
(136, ':hear_no_evil:', 'hear-no-evil'),
(137, ':speak_no_evil:', 'speak-no-evil'),
(138, ':guardsman:', 'guardsman'),
(139, ':skull:', 'skull'),
(140, ':feet:', 'feet'),
(141, ':lips:', 'lips'),
(142, ':kiss:', 'kiss'),
(143, ':droplet:', 'droplet'),
(144, ':ear:', 'ear'),
(145, ':eyes:', 'eyes'),
(146, ':nose:', 'nose'),
(147, ':tongue:', 'tongue'),
(148, ':love_letter:', 'love-letter'),
(149, ':speech_balloon:', 'speech-balloon'),
(150, ':thought_balloon:', 'thought-balloon'),
(151, ':sunny:', 'sunny');

CREATE TABLE `events` (
  `event_id` int(10) unsigned NOT NULL auto_increment,
  `event_privacy` enum('secret','closed','public') NULL DEFAULT 'public',
  `event_admin` int(10) unsigned NOT NULL,
  `event_category` int(10) unsigned NOT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_location` varchar(255) NULL,
  `event_description` text NOT NULL,
  `event_start_date` datetime NOT NULL,
  `event_end_date` datetime NOT NULL,
  `event_cover` varchar(255) NULL,
  `event_cover_id` int(10) unsigned NULL,
  `event_album_covers` int(10) NULL,
  `event_album_timeline` int(10) NULL,
  `event_pinned_post` int(10) NULL,
  `event_invited` int(10) unsigned NOT NULL DEFAULT '0',
  `event_interested` int(10) unsigned NOT NULL DEFAULT '0',
  `event_going` int(10) unsigned NOT NULL DEFAULT '0',
  `event_date` datetime NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

CREATE TABLE `events_categories` (
  `category_id` int(10) unsigned NOT NULL auto_increment,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

INSERT INTO `events_categories` (`category_id`, `category_name`) VALUES
(1, 'Art'),
(2, 'Causes'),
(3, 'Crafts'),
(4, 'Dance'),
(5, 'Drinks'),
(6, 'Film'),
(7, 'Fitness'),
(8, 'Food'),
(9, 'Games'),
(10, 'Gardening'),
(11, 'Health'),
(12, 'Home'),
(13, 'Literature'),
(14, 'Music'),
(15, 'Networking'),
(16, 'Other'),
(17, 'Party'),
(18, 'Religion'),
(19, 'Shopping'),
(20, 'Sports'),
(21, 'Theater'),
(22, 'Wellness');

CREATE TABLE `events_members` (
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `is_invited` enum('0','1') NULL DEFAULT '0',
  `is_interested` enum('0','1') NULL DEFAULT '0',
  `is_going` enum('0','1') NULL DEFAULT '0',
  UNIQUE KEY `group_id_user_id`(`event_id`,`user_id`)
) ENGINE=MyISAM row_format=FIXED;

CREATE TABLE `game_players` (
  `game_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `game_id_user_id`(`game_id`,`user_id`)
) ENGINE=MyISAM row_format=FIXED;

ALTER TABLE `groups`
  ADD COLUMN `group_privacy` enum('secret','closed','public') NULL DEFAULT 'public';

ALTER TABLE `groups_members`
  ADD COLUMN `approved` enum('0','1') NOT NULL DEFAULT '0';

UPDATE `groups_members`
 SET `approved` = '1';

ALTER TABLE `notifications`
  ADD COLUMN `notify_id` varchar(255) NULL;

CREATE TABLE `pages_invites` (
  `page_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `from_user_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `page_id_user_id_from_user_id`(`page_id`,`user_id`,`from_user_id`)
) ENGINE=MyISAM row_format=FIXED;

ALTER TABLE `posts`
  ADD COLUMN `in_event` enum('0','1') NOT NULL DEFAULT '0'
  , ADD COLUMN `event_id` int(10) unsigned NULL;

CREATE TABLE `posts_articles` (
  `article_id` int(10) unsigned NOT NULL auto_increment,
  `post_id` int(10) unsigned NOT NULL,
  `cover` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `tags` text NOT NULL,
  `views` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

ALTER TABLE `posts_comments`
  MODIFY COLUMN `node_type` enum('post','photo','comment') NOT NULL
  , ADD COLUMN `replies` int(10) unsigned NOT NULL DEFAULT '0';

ALTER TABLE `posts_photos_albums`
  MODIFY COLUMN `privacy` enum('me','friends','public','custom') NOT NULL DEFAULT 'public'
  , ADD COLUMN `in_event` enum('0','1') NOT NULL DEFAULT '0'
  , ADD COLUMN `event_id` int(10) unsigned NULL;

CREATE TABLE `stickers` (
  `sticker_id` int(10) unsigned NOT NULL auto_increment,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`sticker_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

INSERT INTO `stickers` (`sticker_id`, `image`) VALUES
(1, 'stickers/1.png'),
(2, 'stickers/2.png'),
(3, 'stickers/3.png'),
(4, 'stickers/4.png'),
(5, 'stickers/5.png'),
(6, 'stickers/6.png'),
(7, 'stickers/7.png'),
(8, 'stickers/8.png'),
(9, 'stickers/9.png'),
(10, 'stickers/10.png'),
(11, 'stickers/11.png'),
(12, 'stickers/12.png'),
(13, 'stickers/13.png'),
(14, 'stickers/14.png'),
(15, 'stickers/15.png'),
(16, 'stickers/16.png'),
(17, 'stickers/17.png'),
(18, 'stickers/18.png');

CREATE TABLE `stories` (
  `story_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `text` longtext NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`story_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

CREATE TABLE `stories_media` (
  `media_id` int(10) unsigned NOT NULL auto_increment,
  `story_id` int(10) unsigned NOT NULL,
  `source` varchar(255) NOT NULL,
  `is_photo` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`media_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

ALTER TABLE `system_options`
  DROP COLUMN `advanced_scraper`
  , MODIFY COLUMN `system_public` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `system_live` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `contact_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `directory_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `pages_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `groups_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `market_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `games_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `verification_requests` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `profile_notification_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `wall_posts_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `geolocation_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `registration_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `packages_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `activation_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `age_restriction` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `getting_started` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `delete_accounts_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `social_login_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `facebook_login_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `twitter_login_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `google_login_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `instagram_login_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `linkedin_login_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `vkontakte_login_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `email_smtp_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `email_smtp_authentication` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `email_smtp_ssl` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `email_notifications` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `email_post_likes` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `email_post_comments` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `email_post_shares` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `email_wall_posts` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `email_mentions` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `email_profile_visits` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `email_friend_requests` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `chat_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `chat_status_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `s3_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `photos_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `videos_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `audio_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `file_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `censored_words_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `reCAPTCHA_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `paypal_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `creditcard_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `alipay_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `bitcoin_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `system_wallpaper_default` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `system_favicon_default` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `system_ogimage_default` enum('0','1') NOT NULL DEFAULT '1'
  , MODIFY COLUMN `css_customized` enum('0','1') NOT NULL DEFAULT '0'
  , MODIFY COLUMN `affiliates_enabled` enum('0','1') NOT NULL DEFAULT '0'
  , ADD COLUMN `events_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `blogs_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `daytime_msg_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `stories_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `social_share_enabled` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `smart_yt_player` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `default_privacy` enum('public','friends','me') NOT NULL DEFAULT 'friends'
  , ADD COLUMN `system_random_profiles` enum('0','1') NOT NULL DEFAULT '1';

ALTER TABLE `users`
  CHANGE COLUMN `user_fullname` `user_firstname` VARCHAR(255) NOT NULL
  , ADD COLUMN `user_lastname` varchar(255) NULL
  , ADD COLUMN `user_privacy_events` enum('me','friends','public') NOT NULL DEFAULT 'public'
  , ADD COLUMN `notifications_sound` enum('0','1') NOT NULL DEFAULT '1';

CREATE TABLE `users_searches` (
  `log_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  `node_type` varchar(32) NOT NULL,
  `time` datetime NULL,
  UNIQUE KEY `node_id_node_type`(`node_id`,`node_type`),
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM row_format=DYNAMIC;
";
    

    $db->multi_query($structure) or _error("Error", $db->error);
    // flush multi_queries
    do{} while(mysqli_more_results($db) && mysqli_next_result($db));


    // [5] update system settings
    $db->query(sprintf("UPDATE system_options SET session_hash = %s", secure($session_hash) )) or _error("Error #103", $db->error);


     // [6] create config file
    $config_string = '<?php  
    define("DB_NAME", "'.DB_NAME. '");
    define("DB_USER", "'.DB_USER. '");
    define("DB_PASSWORD", "'.DB_PASSWORD. '");
    define("DB_HOST", "'.DB_HOST. '");
    define("SYS_URL", "'. get_system_url(). '");
    define("DEBUGGING", false);
    define("DEFAULT_LOCALE", "en_us");
    define("LICENCE_KEY", "'. $licence_key. '");
    
    ?>';
    
    $config_file = 'includes/config.php';
    $handle = fopen($config_file, 'w') or _error("System Error", "Cannot create the config file");
    
    fwrite($handle, $config_string);
    fclose($handle);

    // [6] Done
    _error("System Updated", "Sngine has been updated to 2.5");
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        
        <title>Sngine v2.5 &rsaquo; Update</title>
        
        <link rel="stylesheet" type="text/css" href="includes/assets/js/sngine/installer/installer.css" />
        <script src="includes/assets/js/sngine/installer/modernizr.custom.js"></script>
    </head>

    <body>
        
        <div class="container">

            <div class="fs-form-wrap" id="fs-form-wrap">
                
                <div class="fs-title">
                    <h1>Sngine v2.5 Update</h1>
                </div>
                
                <form id="myform" class="fs-form fs-form-full" autocomplete="off" action="update.php" method="post">
                    <ol class="fs-fields">

                        <li>
                            <p class="fs-field-label fs-anim-upper">
                                Welcome to <strong>Sngine</strong> updating process! Just fill in the information below.
                            </p>
                        </li>

                        <li>
                            <label class="fs-field-label fs-anim-upper" for="purchase_code" data-info="The purchase code of Sngine">Purchase Code</label>
                            <input class="fs-anim-lower" id="purchase_code" name="purchase_code" type="text" placeholder="xxx-xx-xxxx" required/>
                        </li>

                    </ol>
                    <button class="fs-submit" name="submit" type="submit">Update</button>
                </form>

            </div>

        </div>
        
        <script src="includes/assets/js/sngine/installer/classie.js"></script>
        <script src="includes/assets/js/sngine/installer/fullscreenForm.js"></script>
        <script>
            (function() {
                var formWrap = document.getElementById( 'fs-form-wrap' );
                new FForm( formWrap, {
                    onReview : function() {
                        classie.add( document.body, 'overview' );
                    }
                } );
            })();
        </script>

    </body>
</html>