package com.sngine.webview;

public interface OnClickButtonListener {

    void onClickButton(int which);

}
