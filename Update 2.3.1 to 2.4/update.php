<?php
/**
 * Sngine updater
 * 
 * @package Sngine v2+
 * @author Zamblek
 */

// set system version
define('SYS_VER', '2.4');


// set absolut & base path
define('ABSPATH',dirname(__FILE__).'/');
define('BASEPATH',dirname($_SERVER['PHP_SELF']));


// check the config file
if(!file_exists(ABSPATH.'includes/config.php')) {
    /* the config file doesn't exist -> start the installer */
    header('Location: ./install');
}


// get system configurations
require_once(ABSPATH.'includes/config.php');


// enviroment settings
if(DEBUGGING) {
    ini_set("display_errors", true);
    error_reporting(E_ALL ^ E_NOTICE);
} else {
    ini_set("display_errors", false);
    error_reporting(0);
}


// get functions
require_once(ABSPATH.'includes/functions.php');


// connect to the database
$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$db->set_charset('utf8');
if(mysqli_connect_error()) {
    _error(DB_ERROR);
}


// install
if(isset($_POST['submit'])) {

    // [0] check valid purchase code
    /* get licence key */
    try {
        $licence_key = get_licence_key($_POST['purchase_code']);
        if(is_empty($_POST['purchase_code']) || $licence_key === false) {
            _error("Error", "Please enter a valid purchase code");
        }
        /* update session hash for AJAX CSRF security */
        $session_hash = $licence_key;
    } catch (Exception $e) {
        _error("Error", $e->getMessage());
    }
    
    
    // [2] update the sngine tables
    $structure = "

ALTER TABLE `ads`
  MODIFY COLUMN `place` varchar(32) NOT NULL;

CREATE TABLE `affiliates_payments` (
  `payment_id` int(10) NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL,
  `email` varchar(64) NOT NULL,
  `amount` varchar(32) NOT NULL,
  `method` varchar(64) NOT NULL,
  `time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

ALTER TABLE `announcements`
  MODIFY COLUMN `type` varchar(32) NOT NULL;

CREATE TABLE `banned_ips` (
  `ip_id` int(10) unsigned NOT NULL auto_increment,
  `ip` varchar(64) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`ip_id`)
) ENGINE=MyISAM;

CREATE TABLE `custom_fields` (
  `field_id` int(10) unsigned NOT NULL auto_increment,
  `type` varchar(32) NOT NULL,
  `select_options` text NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `place` varchar(32) NOT NULL,
  `length` int(10) NOT NULL DEFAULT '32',
  `mandatory` enum('0','1') NOT NULL DEFAULT '0',
  `in_registration` enum('0','1') NOT NULL DEFAULT '0',
  `in_profile` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

ALTER TABLE `groups`
  MODIFY COLUMN `group_name` varchar(64) NOT NULL
  , MODIFY COLUMN `group_picture` varchar(255) NULL
  , MODIFY COLUMN `group_picture_id` int(10) unsigned NULL
  , MODIFY COLUMN `group_cover` varchar(255) NULL
  , MODIFY COLUMN `group_cover_id` int(10) unsigned NULL
  , MODIFY COLUMN `group_album_pictures` int(10) NULL
  , MODIFY COLUMN `group_album_covers` int(10) NULL
  , MODIFY COLUMN `group_album_timeline` int(10) NULL
  , MODIFY COLUMN `group_pinned_post` int(10) NULL
  , ADD COLUMN `group_date` datetime NULL;

CREATE TABLE `market_categories` (
  `category_id` int(10) unsigned NOT NULL auto_increment,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

ALTER TABLE `notifications`
  MODIFY COLUMN `action` varchar(32) NOT NULL
  , MODIFY COLUMN `node_type` varchar(32) NOT NULL
  , MODIFY COLUMN `time` datetime NULL;

CREATE TABLE `packages` (
  `package_id` int(10) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `price` varchar(32) NOT NULL,
  `period_num` int(10) unsigned NOT NULL,
  `period` varchar(32) NOT NULL,
  `color` varchar(32) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `boost_posts_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `boost_posts` int(10) unsigned NOT NULL,
  `boost_pages_enabled` enum('0','1') NOT NULL DEFAULT '0',
  `boost_pages` int(10) unsigned NOT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

CREATE TABLE `packages_payments` (
  `payment_id` int(10) NOT NULL auto_increment,
  `payment_date` datetime NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `package_price` varchar(32) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

ALTER TABLE `pages`
  MODIFY COLUMN `page_name` varchar(64) NOT NULL
  , MODIFY COLUMN `page_picture` varchar(255) NULL
  , MODIFY COLUMN `page_picture_id` int(10) unsigned NULL
  , MODIFY COLUMN `page_cover` varchar(255) NULL
  , MODIFY COLUMN `page_cover_id` int(10) unsigned NULL
  , MODIFY COLUMN `page_album_pictures` int(10) unsigned NULL
  , MODIFY COLUMN `page_album_covers` int(10) unsigned NULL
  , MODIFY COLUMN `page_album_timeline` int(10) unsigned NULL
  , MODIFY COLUMN `page_pinned_post` int(10) unsigned NULL
  , ADD COLUMN `page_boosted` enum('0','1') NOT NULL DEFAULT '0'
  , ADD COLUMN `page_date` datetime NULL;

ALTER TABLE `posts`
  MODIFY COLUMN `post_type` varchar(32) NOT NULL
  , MODIFY COLUMN `location` varchar(255) NULL
  , MODIFY COLUMN `privacy` varchar(32) NOT NULL
  , MODIFY COLUMN `text` longtext NULL
  , ADD COLUMN `feeling_action` varchar(32) NULL
  , ADD COLUMN `feeling_value` varchar(255) NULL
  , ADD COLUMN `boosted` enum('0','1') NOT NULL DEFAULT '0';

ALTER TABLE `posts_photos`
  MODIFY COLUMN `album_id` int(10) unsigned NULL;

CREATE TABLE `posts_polls` (
  `poll_id` int(10) unsigned NOT NULL auto_increment,
  `post_id` int(10) unsigned NOT NULL,
  `votes` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`poll_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

CREATE TABLE `posts_polls_options` (
  `option_id` int(10) unsigned NOT NULL auto_increment,
  `poll_id` int(10) unsigned NOT NULL,
  `text` varchar(255) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

CREATE TABLE `posts_products` (
  `product_id` int(10) unsigned NOT NULL auto_increment,
  `post_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(32) NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `location` varchar(255) NOT NULL,
  `available` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

CREATE TABLE `RG_TEMP_1779579527_0` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `system_live` enum('1','0') NOT NULL DEFAULT '1',
  `system_message` text NOT NULL,
  `system_title` varchar(255) NOT NULL DEFAULT 'Sngine',
  `system_description` text NOT NULL,
  `system_keywords` text NOT NULL,
  `system_email` varchar(255) NULL,
  `system_public` enum('1','0') NOT NULL DEFAULT '1',
  `directory_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `market_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `pages_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `groups_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `games_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `contact_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `verification_requests` enum('1','0') NOT NULL DEFAULT '1',
  `profile_notification_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `wall_posts_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `advanced_scraper` enum('1','0') NOT NULL DEFAULT '0',
  `geolocation_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `geolocation_key` varchar(255) NULL,
  `registration_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `registration_type` enum('free','paid') NOT NULL DEFAULT 'free',
  `packages_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `activation_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `activation_type` enum('email','sms') NOT NULL DEFAULT 'email',
  `age_restriction` enum('1','0') NOT NULL DEFAULT '0',
  `minimum_age` tinyint(1) unsigned NULL,
  `getting_started` enum('1','0') NOT NULL DEFAULT '1',
  `delete_accounts_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `max_accounts` int(10) unsigned NOT NULL DEFAULT '0',
  `social_login_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `facebook_login_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `facebook_appid` varchar(255) NULL,
  `facebook_secret` varchar(255) NULL,
  `twitter_login_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `twitter_appid` varchar(255) NULL,
  `twitter_secret` varchar(255) NULL,
  `google_login_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `google_appid` varchar(255) NULL,
  `google_secret` varchar(255) NULL,
  `instagram_login_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `instagram_appid` varchar(255) NULL,
  `instagram_secret` varchar(255) NULL,
  `linkedin_login_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `linkedin_appid` varchar(255) NULL,
  `linkedin_secret` varchar(255) NULL,
  `vkontakte_login_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `vkontakte_appid` varchar(255) NULL,
  `vkontakte_secret` varchar(255) NULL,
  `email_smtp_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `email_smtp_authentication` enum('1','0') NOT NULL DEFAULT '1',
  `email_smtp_ssl` enum('1','0') NOT NULL DEFAULT '0',
  `email_smtp_server` varchar(255) NULL,
  `email_smtp_port` varchar(255) NULL,
  `email_smtp_username` varchar(255) NULL,
  `email_smtp_password` varchar(255) NULL,
  `email_smtp_setfrom` varchar(255) NULL,
  `email_notifications` enum('1','0') NOT NULL DEFAULT '1',
  `email_post_likes` enum('1','0') NOT NULL DEFAULT '1',
  `email_post_comments` enum('1','0') NOT NULL DEFAULT '1',
  `email_post_shares` enum('1','0') NOT NULL DEFAULT '1',
  `email_wall_posts` enum('1','0') NOT NULL DEFAULT '1',
  `email_mentions` enum('1','0') NOT NULL DEFAULT '1',
  `email_profile_visits` enum('1','0') NOT NULL DEFAULT '1',
  `email_friend_requests` enum('1','0') NOT NULL DEFAULT '1',
  `twilio_sid` varchar(255) NULL,
  `twilio_token` varchar(255) NULL,
  `twilio_phone` varchar(255) NULL,
  `system_phone` varchar(255) NULL,
  `chat_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `chat_status_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `s3_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `s3_bucket` varchar(255) NULL,
  `s3_region` varchar(255) NULL,
  `s3_key` varchar(255) NULL,
  `s3_secret` varchar(255) NULL,
  `uploads_directory` varchar(255) NOT NULL DEFAULT 'content/uploads',
  `uploads_prefix` varchar(255) NULL DEFAULT 'sngine',
  `max_avatar_size` int(10) unsigned NOT NULL DEFAULT '5120',
  `max_cover_size` int(10) unsigned NOT NULL DEFAULT '5120',
  `photos_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `max_photo_size` int(10) unsigned NOT NULL DEFAULT '5120',
  `videos_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `max_video_size` int(10) unsigned NOT NULL DEFAULT '5120',
  `video_extensions` text NOT NULL,
  `audio_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `max_audio_size` int(10) unsigned NOT NULL DEFAULT '5120',
  `audio_extensions` text NOT NULL,
  `file_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `max_file_size` int(10) unsigned NOT NULL DEFAULT '5120',
  `file_extensions` text NOT NULL,
  `censored_words_enabled` enum('1','0') NOT NULL DEFAULT '1',
  `censored_words` text NOT NULL,
  `reCAPTCHA_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `reCAPTCHA_site_key` varchar(255) NULL,
  `reCAPTCHA_secret_key` varchar(255) NULL,
  `session_hash` varchar(255) NULL,
  `paypal_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `paypal_mode` enum('live','sandbox') NOT NULL DEFAULT 'sandbox',
  `paypal_id` varchar(255) NULL,
  `paypal_secret` varchar(255) NULL,
  `creditcard_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `alipay_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `bitcoin_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `stripe_mode` enum('live','test') NOT NULL DEFAULT 'test',
  `stripe_test_secret` varchar(255) NULL,
  `stripe_test_publishable` varchar(255) NULL,
  `stripe_live_secret` varchar(255) NULL,
  `stripe_live_publishable` varchar(255) NULL,
  `system_currency` varchar(64) NULL DEFAULT 'USD',
  `data_heartbeat` int(10) unsigned NOT NULL DEFAULT '5',
  `chat_heartbeat` int(10) unsigned NOT NULL DEFAULT '5',
  `offline_time` int(10) unsigned NOT NULL DEFAULT '10',
  `min_results` int(10) unsigned NOT NULL DEFAULT '5',
  `max_results` int(10) unsigned NOT NULL DEFAULT '10',
  `min_results_even` int(10) unsigned NOT NULL DEFAULT '6',
  `max_results_even` int(10) unsigned NOT NULL DEFAULT '12',
  `analytics_code` text NULL,
  `system_logo` varchar(255) NULL,
  `system_wallpaper_default` enum('1','0') NOT NULL DEFAULT '1',
  `system_wallpaper` varchar(255) NULL,
  `system_favicon_default` enum('1','0') NOT NULL DEFAULT '1',
  `system_favicon` varchar(255) NULL,
  `system_ogimage_default` enum('1','0') NOT NULL DEFAULT '1',
  `system_ogimage` varchar(255) NULL,
  `css_customized` enum('1','0') NOT NULL DEFAULT '0',
  `css_background` varchar(32) NULL,
  `css_link_color` varchar(32) NULL,
  `css_header` varchar(32) NULL,
  `css_header_search` varchar(32) NULL,
  `css_header_search_color` varchar(32) NULL,
  `css_btn_primary` varchar(32) NULL,
  `css_menu_background` varchar(32) NULL,
  `css_custome_css` text NULL,
  `affiliates_enabled` enum('1','0') NOT NULL DEFAULT '0',
  `affiliate_type` enum('registration','packages') NOT NULL DEFAULT 'registration',
  `affiliate_payment_method` enum('paypal','skrill','both') NOT NULL DEFAULT 'both',
  `affiliates_min_withdrawal` int(10) unsigned NOT NULL DEFAULT '50',
  `affiliates_per_user` varchar(32) NOT NULL DEFAULT '0.1',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM row_format=DYNAMIC;

INSERT INTO `RG_TEMP_1779579527_0`(`ID`,`system_live`,`system_message`,`system_title`,`system_description`,`system_keywords`,`system_public`,`pages_enabled`,`groups_enabled`,`games_enabled`,`profile_notification_enabled`,`wall_posts_enabled`,`geolocation_enabled`,`geolocation_key`,`registration_enabled`,`getting_started`,`delete_accounts_enabled`,`max_accounts`,`social_login_enabled`,`facebook_login_enabled`,`facebook_appid`,`facebook_secret`,`twitter_login_enabled`,`twitter_appid`,`twitter_secret`,`google_login_enabled`,`google_appid`,`google_secret`,`linkedin_login_enabled`,`linkedin_appid`,`linkedin_secret`,`vkontakte_login_enabled`,`vkontakte_appid`,`vkontakte_secret`,`email_smtp_enabled`,`email_smtp_authentication`,`email_smtp_server`,`email_smtp_port`,`email_smtp_username`,`email_smtp_password`,`chat_enabled`,`chat_status_enabled`,`uploads_prefix`,`max_avatar_size`,`max_cover_size`,`photos_enabled`,`max_photo_size`,`videos_enabled`,`max_video_size`,`video_extensions`,`audio_enabled`,`max_audio_size`,`audio_extensions`,`file_enabled`,`max_file_size`,`file_extensions`,`censored_words_enabled`,`censored_words`,`reCAPTCHA_enabled`,`reCAPTCHA_site_key`,`reCAPTCHA_secret_key`,`data_heartbeat`,`chat_heartbeat`,`offline_time`,`min_results`,`max_results`,`min_results_even`,`max_results_even`,`analytics_code`,`system_logo`,`system_favicon_default`,`system_favicon`,`system_ogimage_default`,`system_ogimage`,`css_customized`,`css_background`,`css_link_color`,`css_header`,`css_header_search`,`css_header_search_color`,`css_btn_primary`,`css_menu_background`,`system_email`,`directory_enabled`,`market_enabled`,`contact_enabled`,`verification_requests`,`advanced_scraper`,`registration_type`,`packages_enabled`,`activation_enabled`,`activation_type`,`age_restriction`,`minimum_age`,`instagram_login_enabled`,`instagram_appid`,`instagram_secret`,`email_smtp_ssl`,`email_smtp_setfrom`,`email_notifications`,`email_post_likes`,`email_post_comments`,`email_post_shares`,`email_wall_posts`,`email_mentions`,`email_profile_visits`,`email_friend_requests`,`twilio_sid`,`twilio_token`,`twilio_phone`,`system_phone`,`s3_enabled`,`s3_bucket`,`s3_region`,`s3_key`,`s3_secret`,`uploads_directory`,`session_hash`,`paypal_enabled`,`paypal_mode`,`paypal_id`,`paypal_secret`,`creditcard_enabled`,`alipay_enabled`,`bitcoin_enabled`,`stripe_mode`,`stripe_test_secret`,`stripe_test_publishable`,`stripe_live_secret`,`stripe_live_publishable`,`system_currency`,`system_wallpaper_default`,`system_wallpaper`,`css_custome_css`,`affiliates_enabled`,`affiliate_type`,`affiliate_payment_method`,`affiliates_min_withdrawal`,`affiliates_per_user`) SELECT `ID`,`system_live`,`system_message`,`system_title`,`system_description`,`system_keywords`,`system_public`,`pages_enabled`,`groups_enabled`,`games_enabled`,`profile_notification_enabled`,`wall_posts_enabled`,`geolocation_enabled`,`geolocation_key`,`registration_enabled`,`getting_started`,`delete_accounts_enabled`,`max_accounts`,`social_login_enabled`,`facebook_login_enabled`,`facebook_appid`,`facebook_secret`,`twitter_login_enabled`,`twitter_appid`,`twitter_secret`,`google_login_enabled`,`google_appid`,`google_secret`,`linkedin_login_enabled`,`linkedin_appid`,`linkedin_secret`,`vkontakte_login_enabled`,`vkontakte_appid`,`vkontakte_secret`,`email_smtp_enabled`,`email_smtp_authentication`,`email_smtp_server`,`email_smtp_port`,`email_smtp_username`,`email_smtp_password`,`chat_enabled`,`chat_status_enabled`,`uploads_prefix`,`max_avatar_size`,`max_cover_size`,`photos_enabled`,`max_photo_size`,`videos_enabled`,`max_video_size`,`video_extensions`,`audio_enabled`,`max_audio_size`,`audio_extensions`,`file_enabled`,`max_file_size`,`file_extensions`,`censored_words_enabled`,`censored_words`,`reCAPTCHA_enabled`,`reCAPTCHA_site_key`,`reCAPTCHA_secret_key`,`data_heartbeat`,`chat_heartbeat`,`offline_time`,`min_results`,`max_results`,`min_results_even`,`max_results_even`,`analytics_code`,`system_logo`,`system_favicon_default`,`system_favicon`,`system_ogimage_default`,`system_ogimage`,`css_customized`,`css_background`,`css_link_color`,`css_header`,`css_header_search`,`css_header_search_color`,`css_btn_primary`,`css_menu_background`,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL FROM `system_options`;

DROP TABLE `system_options`;

ALTER TABLE `RG_TEMP_1779579527_0` RENAME TO `system_options`;

ALTER TABLE `users` DROP INDEX `user_token`;

ALTER TABLE `reports`
  MODIFY COLUMN `report_id` int(10) unsigned NOT NULL auto_increment
  , MODIFY COLUMN `user_id` int(10) unsigned NOT NULL
  , MODIFY COLUMN `node_id` int(10) unsigned NOT NULL
  , MODIFY COLUMN `node_type` varchar(32) NOT NULL;

ALTER TABLE `static_pages`
  MODIFY COLUMN `page_url` varchar(64) NOT NULL
  , ADD COLUMN `in_footer` enum('0','1') NOT NULL DEFAULT '1';

ALTER TABLE `system_languages`
  MODIFY COLUMN `code` varchar(32) NOT NULL
  , MODIFY COLUMN `flag_icon` varchar(32) NOT NULL
  , MODIFY COLUMN `enabled` enum('0','1') NOT NULL;

ALTER TABLE `system_themes`
  MODIFY COLUMN `name` varchar(64) NOT NULL;

CREATE TABLE `users_custom_fields` (
  `user_id` int(10) unsigned NOT NULL,
  `field_id` int(10) unsigned NOT NULL,
  `value` text NOT NULL,
  UNIQUE KEY `user_id_field_id`(`user_id`,`field_id`)
) ENGINE=MyISAM row_format=FIXED;

CREATE TABLE `users_polls_options` (
  `user_id` int(10) unsigned NOT NULL,
  `poll_id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `user_id_poll_id`(`user_id`,`poll_id`)
) ENGINE=MyISAM row_format=FIXED;

CREATE TABLE `users_sessions` (
  `session_id` int(10) unsigned NOT NULL auto_increment,
  `session_token` varchar(64) NOT NULL,
  `session_date` datetime NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_browser` varchar(64) NOT NULL,
  `user_os` varchar(64) NOT NULL,
  `user_ip` varchar(64) NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_token`(`session_token`),
  KEY `user_ip`(`user_ip`)
) ENGINE=MyISAM row_format=DYNAMIC;

CREATE TABLE `verification_requests` (
  `request_id` int(10) unsigned NOT NULL auto_increment,
  `node_id` int(10) unsigned NOT NULL,
  `node_type` varchar(32) NOT NULL,
  `time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  UNIQUE KEY `node_id_node_type`(`node_id`,`node_type`),
  PRIMARY KEY (`request_id`)
) ENGINE=MyISAM row_format=DYNAMIC;

ALTER TABLE `widgets`
  MODIFY COLUMN `widget_id` int(10) unsigned NOT NULL auto_increment
  , MODIFY COLUMN `place` varchar(32) NOT NULL;


ALTER TABLE `users`
  DROP COLUMN `user_token`
  , DROP COLUMN `user_blocked`
  , DROP COLUMN `user_ip`
  , MODIFY COLUMN `user_group` tinyint(10) unsigned NOT NULL DEFAULT 3
  , MODIFY COLUMN `user_email` varchar(64) NOT NULL
  , MODIFY COLUMN `user_activation_key` varchar(64) NULL
  , MODIFY COLUMN `user_reset_key` varchar(64) NULL
  , MODIFY COLUMN `user_live_requests_counter` int(10) unsigned NOT NULL DEFAULT '0'
  , MODIFY COLUMN `user_live_requests_lastid` int(10) unsigned NOT NULL DEFAULT '0'
  , MODIFY COLUMN `user_live_messages_counter` int(10) unsigned NOT NULL DEFAULT '0'
  , MODIFY COLUMN `user_live_messages_lastid` int(10) unsigned NOT NULL DEFAULT '0'
  , MODIFY COLUMN `user_live_notifications_counter` int(10) unsigned NOT NULL DEFAULT '0'
  , MODIFY COLUMN `user_live_notifications_lastid` int(10) unsigned NOT NULL DEFAULT '0'
  , MODIFY COLUMN `user_picture` varchar(255) NULL
  , MODIFY COLUMN `user_picture_id` int(10) unsigned NULL
  , MODIFY COLUMN `user_cover` varchar(255) NULL
  , MODIFY COLUMN `user_cover_id` int(10) unsigned NULL
  , MODIFY COLUMN `user_album_pictures` int(10) unsigned NULL
  , MODIFY COLUMN `user_album_covers` int(10) unsigned NULL
  , MODIFY COLUMN `user_album_timeline` int(10) unsigned NULL
  , MODIFY COLUMN `user_pinned_post` int(10) unsigned NULL
  , MODIFY COLUMN `user_registered` datetime NULL
  , MODIFY COLUMN `user_last_login` datetime NULL
  , MODIFY COLUMN `user_work_title` varchar(255) NULL
  , MODIFY COLUMN `user_work_place` varchar(255) NULL
  , MODIFY COLUMN `user_current_city` varchar(255) NULL
  , MODIFY COLUMN `user_hometown` varchar(255) NULL
  , MODIFY COLUMN `user_edu_major` varchar(255) NULL
  , MODIFY COLUMN `user_edu_school` varchar(255) NULL
  , MODIFY COLUMN `user_edu_class` varchar(255) NULL
  , ADD COLUMN `user_phone` varchar(64) NULL
  , ADD COLUMN `user_subscribed` enum('0','1') NOT NULL DEFAULT '0'
  , ADD COLUMN `user_package` int(10) unsigned NULL
  , ADD COLUMN `user_subscription_date` datetime NULL
  , ADD COLUMN `user_boosted_posts` int(10) unsigned NOT NULL DEFAULT '0'
  , ADD COLUMN `user_boosted_pages` int(10) unsigned NOT NULL DEFAULT '0'
  , ADD COLUMN `user_banned` enum('0','1') NOT NULL DEFAULT '0'
  , ADD COLUMN `user_relationship` varchar(255) NULL
  , ADD COLUMN `user_biography` text NULL
  , ADD COLUMN `user_website` varchar(255) NULL
  , ADD COLUMN `user_social_facebook` varchar(255) NULL
  , ADD COLUMN `user_social_twitter` varchar(255) NULL
  , ADD COLUMN `user_social_google` varchar(255) NULL
  , ADD COLUMN `user_social_youtube` varchar(255) NULL
  , ADD COLUMN `user_social_instagram` varchar(255) NULL
  , ADD COLUMN `user_social_linkedin` varchar(255) NULL
  , ADD COLUMN `user_social_vkontakte` varchar(255) NULL
  , ADD COLUMN `user_privacy_relationship` enum('me','friends','public') NOT NULL DEFAULT 'public'
  , ADD COLUMN `user_privacy_basic` enum('me','friends','public') NOT NULL DEFAULT 'public'
  , ADD COLUMN `user_privacy_other` enum('me','friends','public') NOT NULL DEFAULT 'public'
  , ADD COLUMN `email_post_likes` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `email_post_comments` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `email_post_shares` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `email_wall_posts` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `email_mentions` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `email_profile_visits` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `email_friend_requests` enum('0','1') NOT NULL DEFAULT '1'
  , ADD COLUMN `instagram_connected` enum('0','1') NOT NULL DEFAULT '0'
  , ADD COLUMN `instagram_id` varchar(255) NULL
  , ADD COLUMN `user_referrer_id` int(10) NULL
  , ADD COLUMN `user_affiliate_balance` varchar(64) NOT NULL DEFAULT '0';

ALTER TABLE `users` ADD UNIQUE KEY `instagram_id`(`instagram_id`);

ALTER TABLE `users` ADD UNIQUE KEY `user_phone`(`user_phone`);
";
    

    $db->multi_query($structure) or _error("Error", $db->error);
    // flush multi_queries
    do{} while(mysqli_more_results($db) && mysqli_next_result($db));


    // [5] update system settings
    $db->query(sprintf("UPDATE system_options SET session_hash = %s, uploads_directory = 'content/uploads', system_currency = 'USD'", secure($session_hash) )) or _error("Error #103", $db->error);


     // [6] create config file
    $config_string = '<?php  
    define("DB_NAME", "'.DB_NAME. '");
    define("DB_USER", "'.DB_USER. '");
    define("DB_PASSWORD", "'.DB_PASSWORD. '");
    define("DB_HOST", "'.DB_HOST. '");
    define("SYS_URL", "'. get_system_url(). '");
    define("DEBUGGING", false);
    define("DEFAULT_LOCALE", "en_us");
    define("LICENCE_KEY", "'. $licence_key. '");
    
    ?>';
    
    $config_file = 'includes/config.php';
    $handle = fopen($config_file, 'w') or _error("System Error", "Cannot create the config file");
    
    fwrite($handle, $config_string);
    fclose($handle);

    // [6] Done
    _error("System Updated", "Sngine has been updated to 2.4");
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        
        <title>Sngine v2.4 &rsaquo; Update</title>
        
        <link rel="stylesheet" type="text/css" href="includes/assets/js/sngine/installer/installer.css" />
        <script src="includes/assets/js/sngine/installer/modernizr.custom.js"></script>
    </head>

    <body>
        
        <div class="container">

            <div class="fs-form-wrap" id="fs-form-wrap">
                
                <div class="fs-title">
                    <h1>Sngine v2.4 Update</h1>
                </div>
                
                <form id="myform" class="fs-form fs-form-full" autocomplete="off" action="update.php" method="post">
                    <ol class="fs-fields">

                        <li>
                            <p class="fs-field-label fs-anim-upper">
                                Welcome to <strong>Sngine</strong> updating process! Just fill in the information below.
                            </p>
                        </li>

                        <li>
                            <label class="fs-field-label fs-anim-upper" for="purchase_code" data-info="The purchase code of Sngine">Purchase Code</label>
                            <input class="fs-anim-lower" id="purchase_code" name="purchase_code" type="text" placeholder="xxx-xx-xxxx" required/>
                        </li>

                    </ol>
                    <button class="fs-submit" name="submit" type="submit">Update</button>
                </form>

            </div>

        </div>
        
        <script src="includes/assets/js/sngine/installer/classie.js"></script>
        <script src="includes/assets/js/sngine/installer/fullscreenForm.js"></script>
        <script>
            (function() {
                var formWrap = document.getElementById( 'fs-form-wrap' );
                new FForm( formWrap, {
                    onReview : function() {
                        classie.add( document.body, 'overview' );
                    }
                } );
            })();
        </script>

    </body>
</html>